# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  # define a hash to
  # find the length of each word and add pairs
  # return hash
  word_lengths = {}
  str.split(" ").each {|word| word_lengths[word] = word.length}
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  # sort by values
  # return the key of the last array
  (hash.sort_by {|k, v| v})[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k, v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  # create a hash to returns
  # isolate each individual letter in the word
  # find the count each occurs in the word and add to hash
  # return hash
  letter_occurs = Hash.new(0)
  each_letter = word.chars.uniq
  each_letter.each do |let|
    letter_occurs[let] = word.count(let)
  end
  letter_occurs
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = {}
  arr.each {|el| uniq_hash[el] = nil}
  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  # create a return hash
  # test each numbers parity
  num_even_odd = Hash.new(0)
  numbers.each do |num|
    #set up conditional for even and odd
    #add one to value for whichever parity == true
    if num.odd?
      num_even_odd[:odd] += 1
    else
      num_even_odd[:even] += 1
    end
  end
  num_even_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  # create a counter hash
  # create a new string of only vowels from string
  # iterate through characters of new string with counter
  # sort by value and return the first element of the last array
  vowel_num = Hash.new(0)
  vowels = take_vowels(string)

  vowels.each {|v| vowel_num[v] += 1}
  vowel_num.sort_by {|k, v| v}[-1].first
end

def take_vowels(string)
  # define vowels variable
  # return string with only those values
  vowels = "aeiou"
  string.downcase.chars.select {|ch| vowels.include?(ch)}
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  # create return array
  # find and combine all students with values 7+
  #return only unique pairs
  return_array = []
  students.each {|k, v| v > 6 ? return_array << k : next}
  return_array.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  # find the number of unique animals
  # sort by their # occurences to get the first and last
  specimen_pop = Hash.new(0)
  specimens.each {|specimen| specimen_pop[specimen] +=1}
  pop_size = specimen_pop.sort_by {|k, v| v}
  pop_size.length ** 2 * (pop_size[0].last / pop_size[-1].last)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  # create a hash from normal sign with letters as keys and occcurences as values
  # find each unique letter in vandalized_sign
  # return true if if each letter occurs no more than the respectiv letters value in normal_sign
  available_letters = num_of_letters(normal_sign)
  vandalized_sign.downcase.chars.uniq.all? do |let|
    vandalized_sign.downcase.count(let) <= available_letters.values_at(let)[0]
  end
end

def num_of_letters(str)
  #creates a hash of letters with # of occurences as values
  available_letters = Hash.new(0)
  str.downcase.chars.each {|let| available_letters[let] += 1}
  available_letters
end
